<?php

namespace Drupal\my_route_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_route_demo\Controller\HelloController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class HelloController.
 *
 * @package Drupal\my_route_demo\Controller
 */
class HelloController extends ControllerBase {

  /**
   * Hello.
   *
   * @return array
   *   Render array containing our message.
   */
  public function hello() {
    return [
      '#markup' => $this->t('Hello. It is a fine day indeed!'),
    ];
  }

  /**
   * Hello name.
   *
   * @param string $first_name
   *   The first name of the person we're saying hello to.
   * @param string $last_name
   *   The last name of the person we're saying hello to.
   *
   * @return array
   *   Render array containing our message.
   */
  public function helloName($first_name, $last_name) {
    return [
      '#markup' => $this->t('Hello @first_name @last_name', [
        '@first_name' => $first_name,
        '@last_name' => $last_name,
      ]),
    ];
  }

  /**
   * Addition.
   *
   * @param string $num_1
   *   The first number we're adding up.
   * @param string $num_2
   *   The second number we're adding up.
   *
   * @return array
   *   Render array containing the results of the addition.
   */
  public function addition($num_1, $num_2) {
    return [
      '#markup' => $this->t('Total is @total', [
        '@total' => $num_1 + $num_2,
      ]),
    ];
  }

}
